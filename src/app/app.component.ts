import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularHttp';

  constructor() {
    type HttpResponse = {code:number , data:string} ;
  
    const observable = new Observable<HttpResponse>(subscriber => {
      console.log('Inside subscriber..');
      subscriber.next({code: 200, data: 'this is data 1...'});
      subscriber.next({code: 200, data: 'this is data 2...'});
      subscriber.next({code: 200, data: 'this is data 3...'});
      setTimeout(() => {
        subscriber.next({ code: 200, data: 'this is data more data...'});
        subscriber.complete();
      })
    }) ;

    observable.subscribe({
      next(response: HttpResponse) {
        console.log('get Response: ', response);
      },
      error(error: any) {
        console.log('something wrong occured: ', error);
      },
      complete() {
        console.log('done');
      }
    }) ;
  }
}


